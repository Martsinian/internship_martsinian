import xml.etree.ElementTree as ET


class Script4:
    file_name = "annotations.xml"
    doc = ET.parse(file_name)
    root = doc.getroot()

    def change_id(self):    # 7 Изменить id-шники изображений - сделать их в обратном порядке.
        for image in self.root.iter("image"):
            id_inv = str(image.attrib["id"])[::-1]
            image.set('id', f'{id_inv}')
        self.doc.write("output_" + self.file_name, encoding="utf-8")
        print("ID изменены")

    def change_name_to_png(self):   # Изменить name изображений - поменять расширение на 'png'.
        for image in self.root.iter("image"):
            image_name = image.attrib["name"]
            index = image_name.index(".")
            if image_name[index:] == ".jpg":
                image.set('name', f'{image_name[:index] + ".png"}')
        self.doc.write("output_" + self.file_name, encoding="utf-8")
        print("Расширения изменены")

    def change_link_to_name(self):   # Изменить name изображений - удалить путь к файлу.
        for image in self.root.iter("image"):
            image_name = image.attrib["name"]
            index_file = image_name.rfind("/")
            image.set('name', f'{image_name[index_file + 1:]}')
        self.doc.write("output_" + self.file_name, encoding="utf-8")
        print("Пути удалены")


if __name__ == '__main__':
    script = Script4()
    script.change_name_to_png()
    script.change_id()
    script.change_link_to_name()

