import xml.dom.minidom


class Script1:
    image_gty = 0                # кол-во изображений
    image_with_polygon_gty = 0   # кол-во изображений с полигонами
    image_with_box_gt = 0        # кол-во изображений с боксами
    image_is_not_mark = 0        # кол-во изображений без task_1
    polygon_gty = 0              # кол-во полигонов
    box_gty = 0                  # кол-во боксов
    px_list = []                 # список точек
    px_dict = {}                 # словарь точек

    def pars(self):
        doc = xml.dom.minidom.parse("annotations-2.xml")
        image = doc.getElementsByTagName("image")

        for i in image:
            self.image_gty += 1
            polygons = i.getElementsByTagName("polygon")
            boxes = i.getElementsByTagName("box")
            if polygons:
                self.image_with_polygon_gty += 1
            elif boxes:
                self.image_with_box_gt += 1
            else:
                self.image_is_not_mark += 1
            for polygon in polygons:
                self.polygon_gty += 1
            for box in boxes:
                self.box_gty += 1

            width = i.getAttribute("width")
            height = i.getAttribute("height")
            index_file = i.getAttribute("name").rfind("/")
            image_name = i.getAttribute("name")[index_file + 1:]
            px = int(width) * int(height)
            self.px_list.append(px)
            self.px_dict[f"{px}"] = [f"{width} x {height}", image_name]

    def ans(self):
        print(f"\n{'_' * 60}\n")
        print(f"1) Всего изображений:{self.image_gty}")
        print(f"2) Всего изображений с разметкой:{self.image_with_polygon_gty + self.image_with_box_gt}")
        if self.image_is_not_mark != 0:
            print(f"3) Всего изображений без разметки:{self.image_is_not_mark}")
        print(f"5) Всего фигур:{self.polygon_gty + self.box_gty}")
        print(f"6) Самое большое разрешение: {self.px_dict[str(max(self.px_list))][0]};\n"
              f"   Кол-во изображений с таким разрешением: {self.px_list.count(max(self.px_list))};\n"
              f"   Название файла с таким разрешением: {self.px_dict[str(max(self.px_list))][1]}.")
        print(f"   Самое маленькое разрешение: {self.px_dict[str(min(self.px_list))][0]};\n"
              f"   Кол-во изображений с таким разрешением: {self.px_list.count(min(self.px_list))};\n"
              f"   Название файла с таким разрешением: {self.px_dict[str(min(self.px_list))][1]}.")
        print(f"\n{'_' * 60}\n")


if __name__ == '__main__':
    s = Script1()
    s.pars()
    s.ans()
