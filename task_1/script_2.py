import xml.dom.minidom


class Script2:
    polygon_gty = 0        # кол-во полигонов
    box_gty = 0            # кол-во боксов
    class_list = []        # список классов
    class_set = ()         # множество классов

    def class_statistics(self):    # Статистика по классам
        doc = xml.dom.minidom.parse("annotations.xml")
        image = doc.getElementsByTagName("image")

        for i in image:
            polygons = i.getElementsByTagName("polygon")
            boxes = i.getElementsByTagName("box")
            for polygon in polygons:
                self.polygon_gty += 1
                self.class_list.append(polygon.getAttribute("label"))

            for box in boxes:
                self.box_gty += 1
                self.class_list.append(box.getAttribute("label"))
            self.class_set = set(self.class_list)

    def ans(self):
        print(f"\n{'_' * 60}\n")
        print(f"4) Всего фигур: {(self.polygon_gty + self.box_gty)}")
        for it in self.class_set:
            print(f"   Класс {it}: {self.class_list.count(it)}")

        print(f"\n{'_' * 60}\n")


if __name__ == '__main__':
    s = Script2()
    s.class_statistics()
    s.ans()
