import xml.dom.minidom


class Script3:
    polygon_gty = 0         # кол-во полигонов
    box_gty = 0             # кол-во боксов

    def figures_qty_statistics(self):   # Статистика по фигурам
        doc = xml.dom.minidom.parse("annotations.xml")
        image = doc.getElementsByTagName("image")

        for i in image:
            polygons = i.getElementsByTagName("polygon")
            boxes = i.getElementsByTagName("box")
            for polygon in polygons:
                self.polygon_gty += 1
            for box in boxes:
                self.box_gty += 1

    def ans(self):
        print(f"\n{'_' * 60}\n")
        print(f"5*) Всего полигонов:{self.polygon_gty}")
        print(f"5*) Всего боксов:{self.box_gty}")
        print(f"\n{'_' * 60}\n")


if __name__ == '__main__':
    s = Script3()
    s.figures_qty_statistics()
    s.ans()
