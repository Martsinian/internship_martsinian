import xml.dom.minidom
from PIL import Image, ImageDraw


def pars():
    doc = xml.dom.minidom.parse("masks.xml")
    images = doc.getElementsByTagName("image")
    image_quality = 0       # счетчик обработанных изображений
    for image in images:
        index_file = image.getAttribute("name").rfind("/")
        image_name = image.getAttribute("name")[index_file + 1:]   # имя файла
        width = image.getAttribute("width")
        height = image.getAttribute("height")      # разрешение изображения
        try:
            on_photo = Image.open(f"images/{image_name}")   # открываем фото
        except Exception as e:
            print(f"Ошибка: {e} Изображение: {image_name}")
        else:
            print(f"Файл найден. Изображение: {image_name}")
            image_draw = ImageDraw.Draw(on_photo)
            on_dark = Image.new('RGB', (int(width), int(height)), (0, 0, 0))  # создаем черный фон исходя из разрешения
            dark_draw = ImageDraw.Draw(on_dark)

            polygons = image.getElementsByTagName("polygon")
            for polygon in polygons:
                if polygon.getAttribute("label") == "Ignore":   # игнорируем класс Ignore
                    continue
                polygon_data = (polygon.getAttribute("points").replace(";", ","))
                polygon_data_tuple = tuple(map(float, polygon_data.split(",")))  # формируем кортеж координат
                image_draw.polygon(
                    xy=polygon_data_tuple, fill='purple')   # "рисуем" полигон на фото
                dark_draw.polygon(
                    xy=polygon_data_tuple, fill='purple')   # "рисуем" полигон на черном фоне

            on_photo.save(f'photo_{image_name}', quality=95)
            on_dark.save(f'dark_{image_name}', quality=95)   # сохраняем
            image_quality += 1

    print(f"Готово! Всего обработано изображений: {image_quality}")


if __name__ == '__main__':
    pars()
